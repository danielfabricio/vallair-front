var gulp = require('gulp');
var npm = 'node_modules';

// ESTILO
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');

// SCRIPT
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

// Styles sass compile
gulp.task('sass', function () {
  return gulp.src([
        'assets/sass/*.scss'
    ])

    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('../assets/css'));
});

// Javascript LIB Functions
gulp.task('vendor', function() {
    return gulp.src([
        npm + '/jquery/dist/jquery.min.js',
        npm + '/slick-carousel/slick/slick.min.js',
        npm + '/jquery-mask-plugin/dist/jquery.mask.min.js',
        npm + '/ladda/dist/spin.min.js',
        npm + '/ladda/dist/ladda.min.js',
        npm + '/ladda/dist/ladda.jquery.min.js',
        npm + '/plyr/dist/plyr.min.js'
    ])

    .pipe(concat('vendor.js'))
    .pipe(uglify({
        mangle: false
    }))
    .pipe(gulp.dest('../assets/js/'));
});

// Styles Vendor
gulp.task('vendorStyles', function() {
    return gulp.src([
        npm + '/slick-carousel/slick/slick-theme.css',
        npm + '/ladda/dist/ladda.min.css',
        npm + '/plyr/dist/plyr.css'
    ])

    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('../assets/css/'));
});

// Javascript assets
gulp.task('scripts', function () {
    return gulp.src([
        'assets/js/app.js',
        'assets/js/pagination.js'
    ])
    .pipe(concat('app.js'))
    .pipe(uglify({
        mangle: false
    }))
    .pipe(gulp.dest('../assets/js'));
});

 
gulp.task('default', ['vendorStyles', 'scripts', 'vendor', 'sass']);

gulp.task('watch', function () {
    gulp.watch('assets/sass/**/*.scss', ['sass']);
    gulp.watch('assets/js/*.js', ['scripts']);
});