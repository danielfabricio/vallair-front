;(function( window, document, $, undefined ) {
	'use strict';

	var app = (function() {
		var $private = {};
		var $public = {};

		// Banners home
		$public.initBanner = function() {
			if ($('#bannerHome').length) {
				$('#bannerHome').slick({
					arrows: false,
					dots: true,
					speed: 300,
					infinite: false,
					slidesToShow: 1,
					slidesToScroll: 1
				});
			}
		};


		// Blog posts home
		$public.initBlogPosts = function() {
			if ($('#blogPosts').length) {
				$('#blogPosts').slick({
					arrows: false,
					infinite: false,
					dots: false,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					variableWidth: true
				});
			}
		};


		// Partners Carousel
		$public.initPartners = function() {
			if ($('#partnersCarousel').length) {
				$('#partnersCarousel').slick({
					arrows: false,
					dots: true,
					speed: 300,
					variableWidth: true,
					slidesToShow: 1,
					slidesToScroll: 1
				});
			}
		};


		// Produtos Carousel
		$public.initProdcuts = function(){
			if ($('#productsCarousel').length) {
				$('#productsCarousel').slick({
					arrows: false,
					dots: false,
					infinite: false,
					speed: 300,
					variableWidth: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					responsive: [
						{
						  	breakpoint: 540,
						  	settings: {
								variableWidth: false
						  	}
						}
					]
				});
			}


			if ($('.l-grid__produtos__list').length) {
				$('.l-grid__produtos__list').slick({
					arrows: false,
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					mobileFirst: true,
					responsive: [
						{
						  	breakpoint: 541,
						  	settings: 'unslick'
						}
					]
				});
			}


			if ($('.l-grid__custom-products .dd-list').length) {
				$('.l-grid__custom-products .dd-list').slick({
					arrows: false,
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					variableWidth: true
				});
			}

			if ($('.l-container__certificates__list .dd-list').length) {
				$('.l-container__certificates__list .dd-list').slick({
					arrows: false,
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 2,
					slidesToScroll: 2,
					responsive: [
						{
						  	breakpoint: 540,
						  	settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								dots: true,
							}
						}
					]
				});
			}
		};

		// Produtos slide
		$public.initSlideProducts = function() {
			if ($('#produtosSlide').length) {
				$('#produtosSlide').slick({
					arrows: false,
					dots: true,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					variableWidth: true
				});
			}
		};


		// Search menu
		$public.initSearchMenu = function() {
			$('.l-header__search-btn').on('click', function(){
				if ($('.l-header__row--search').hasClass('is-open')) {
					$('.l-header__row--search').slideUp('fast').removeClass('is-open');
				} else {
					$('.l-header__row--search').slideDown('fast').addClass('is-open');
				}
			});
		};


		// Custom Inputs
		$public.customInputs = function() {
			$('.m-form__input').on('click', function(){
				if (!$(this).hasClass('is-focused')) {
					$(this).addClass('is-focused');
				}
			});

			$('.m-form__input').on('focusout', function() {
				if (!$(this).find('.dd-input').val() || $(this).find('.dd-input').val() == '') {
					$(this).removeClass('is-focused');
				}
			});
		};


		// Menus
		$public.initMenu = function() {
			$('#openMenuMobile').on('click', function(){
				$('body').addClass('menu-mode');
			});


			$('.l-header__menu-mobile .menu-item-has-children').on('click', function(e) {
				e.preventDefault();

				$(this).closest('li').toggleClass('is-open');
			});


			$('.dd-btn-close, .l-header__menu-overlay').on('click', function(){
				$('body').removeClass('menu-mode');
			});


			$('.l-header__navigation .menu-item-has-children').on('click', function(e) {
				e.preventDefault();

				$(this).closest('li').toggleClass('is-open');
			});
		};


		// Video
		$public.initVideo = function() {
			var player = new Plyr('#player', {
				controls: ['play-large'],
				ratio: '16:10'
			});

			player.on('play', function(event){
				$('.l-grid__top-video-banner .dd-overlay, .l-grid__top-video-banner .dd-content').hide();
			});

			player.on('pause', function(event){
				$('.l-grid__top-video-banner .dd-overlay, .l-grid__top-video-banner .dd-content').show();
			});


			$('.l-grid__top-video-banner .dd-overlay, .l-grid__top-video-banner .dd-content').on('click', function(){
				player.play();
			});
		};
      
		return $public;
	})();

	// Global
    window.app = app;
    
    $(document).ready(function() {
		app.initMenu();
		app.initBanner();
		app.initBlogPosts();
		app.initSearchMenu();
		app.initPartners();
		app.initProdcuts();
		app.customInputs();
		app.initSlideProducts();
		app.initVideo();
    });

})( window, document, jQuery );