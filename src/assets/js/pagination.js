;(function( window, document, $, undefined ) {
	'use strict';

	var pagination = (function() {
		var $private = {};
		var $public = {};

		// Load More
		$public.loadMore = function() {
            
            $('.dd-btn-loadmore').on('click', function(){
                var button = $(this);
                var list = $(this).closest('.dd-content-box-list');
                var page = parseInt(list.data('page'));
                var type = list.data('type');
                var data = {
                        'action': 'loadmore',
                        'page': page,
                        'type': type
                    }

                $.ajax({
                    url : custom_loadmore_params.ajaxurl, // AJAX handler
                    data : data,
                    type : 'POST',
                    beforeSend : function ( xhr ) {
                        
                    },
                    success : function( data ){
                        if( data ) {
                            
                            list.find('.dd-content-list').append(data);
                            list.data('page', page + 1);

                        } else {
                            button.remove(); // if no data, remove the button as well
                        }
                    }
                });
            });
		};
      
		return $public;
	})();

	// Global
    window.pagination = pagination;
    
    $(document).ready(function() {
		pagination.loadMore();
    });

})( window, document, jQuery );