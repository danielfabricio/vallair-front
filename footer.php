		<!-- footer -->
		<footer class="l-footer">
			<div class="l-container__wrapper">
				<div class="l-footer__content">
					<div class="l-footer__col">
						<h2 class="dd-title dd-title--big">Vallair do Brasil</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis semper.</p>
					</div>

					<div class="l-footer__col l-footer__col--custom-menu">
						<h2 class="dd-title">Bombas</h2>

						<nav>
							<ul>
								<li>
									<a>Pneumáticas</a>

									<ul class="sub-menu">
										<li>
											<a>Bomba Pneumática BPV</a>
										</li>
										<li>
											<a>Bomba Pneumática BPV Especiais</a>
										</li>
										<li>
											<a>Bomba Pneumática Libélula P</a>
										</li>
									</ul>
								</li>

								<li>
									<a>Peristálticas</a>

									<ul class="sub-menu">
										<li>
											<a>Bomba Peristáltica Vallair-Ragazzini</a>
										</li>
										<li>
											<a>Bomba Peristáltica Vallair-Verderflex</a>
										</li>
										<li>
											<a>Bomba Peristáltica Vallair-Stenner</a>
										</li>
									</ul>
								</li>

							</ul>
						</nav>
					</div>

					<div class="l-footer__col l-footer__col--custom-menu">
						<h2 class="dd-title">Bombas</h2>

						<nav>
							<ul>
								<li>
									<a>Pneumáticas</a>

									<ul>
										<li>
											<a>Bomba Pneumática BPV</a>
										</li>
										<li>
											<a>Bomba Pneumática BPV Especiais</a>
										</li>
										<li>
											<a>Bomba Pneumática Libélula P</a>
										</li>
									</ul>
								</li>

								<li>
									<a>Peristálticas</a>

									<ul>
										<li>
											<a>Bomba Peristáltica Vallair-Ragazzini</a>
										</li>
										<li>
											<a>Bomba Peristáltica Vallair-Verderflex</a>
										</li>
										<li>
											<a>Bomba Peristáltica Vallair-Stenner</a>
										</li>
									</ul>
								</li>

							</ul>
						</nav>
					</div>

					<div class="l-footer__col">
						<nav>
							<ul class="dd-socials">
								<li>
									<a href="">
										<i class="dd-icon icon-facebook"></i>
									</a>
								</li>

								<li>
									<a href="">
										<i class="dd-icon icon-twitter"></i>
									</a>
								</li>

								<li>
									<a href="">
										<i class="dd-icon icon-youtube"></i>
									</a>
								</li>

								<li>
									<a href="">
										<i class="dd-icon icon-facebook"></i>
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>

				<div class="l-footer__copyright">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis semper.
				</div>


				<div class="l-footer__phone">
					<div class="l-container__full-row dd-content">
						<a href="" class="m-button m-button--custom">
							<i class="dd-icon icon-phone"></i>

							<div class="dd-text">
								<span class="dd-label">fale conosco</span>
								<span class="dd-phone">(11) 2696-3411</span>
							</div>
						</a>
					</div>
				</div>

			</div>
		</footer>
		<!-- /footer -->

		<?php wp_footer(); ?>

	</body>
</html>
