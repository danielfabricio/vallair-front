<?php /* Template Name: Orcamento Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">

        <!-- #section - Container -->
        <section class="l-grid__orcamento">
            <div class="l-container__wrapper">
                <div class="l-grid__orcamento__content">
                    <div class="l-grid__orcamento__intro">
                        <h1 class="dd-container-title">VALLAIR DO BRASIL <strong><span>|</span> ORÇAMENTO</strong></h1>

                        <div class="dd-container-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tortor massa. 
                                Sed sagittis, nisi at mattis tincidunt, justo nisl pulvinar orci, eget dictum 
                                urna ante at enim. Aliquam turpis lorem, finibus eu enim sed, tempus auctor dolor. 
                                Quisque sit amet nisl eu purus accumsan varius nec ac nisl. In at justo pulvinar, 
                                sodales risus nec, venenatis ipsum.</p>
                        </div>
                    </div>

                    <div class="dd-col dd-col--content">
                        <?php if (isset($_GET['produto'])): ?>
                            <div class="dd-product-info">
                                <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() .'/assets/images/montagem/intro-thumb.png'; ?>" alt=""/>

                                <div class="dd-content">
                                    <p class="dd-intro">Solicitando orçamento para:</p>
                                    <h2 class="dd-title">Bomba Pneumática BPV Lorem ipsum</h2>

                                    <div class="dd-text">
                                        <p>Vazões: até 52.680 l/h</p>
                                        <p>Pressões: até 15,7 bar</p>
                                    </div>
                                </div>

                                <div class="dd-form">
                                    <input class="m-form__input custom-placeholder" name="_finalidade" placeholder="Finalidade de uso"/>
                                    <input class="m-form__input custom-placeholder" name="_quantidade" placeholder="Quantidade"/>
                                </div>
                            </div>
                        <?php else: ?>
                            <h2 class="dd-title no-transform">Precisa de ajuda para escolher o equipamento certo?</h2>
                            
                            <div class="dd-text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut tortor massa. 
                                    Sed sagittis, nisi at mattis tincidunt, justo nisl pulvinar orci, eget dictum urna ante at enim. 
                                    Aliquam turpis lorem, finibus eu enim sed, tempus auctor dolor. Quisque sit amet nisl eu purus 
                                    accumsan varius nec ac nisl. In at justo pulvinar, sodales risus nec, venenatis ipsum.</p>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="dd-col dd-col--form">
                        <h2 class="dd-title">Seus Dados</h2>

                        <div class="m-form">
                            <form>
                                <div class="m-form__input">
                                    <label class="dd-label" for="">Seu nome</label>
                                    <input class="dd-input" type="text" name="nome"/>
                                </div>

                                <div class="m-form__input">
                                    <label class="dd-label" for="">Email</label>
                                    <input class="dd-input" type="text" name="email"/>
                                </div>

                                <div class="m-form__input">
                                    <label class="dd-label" for="">Telefone</label>
                                    <input class="dd-input" type="text" name="telefone"/>
                                </div>

                                <div class="m-form__input m-form__input--textarea">
                                    <textarea class="dd-textarea" type="text" name="mensagem" placeholder="Mensagem"></textarea>
                                </div>

                                <input class="dd-submit" type="submit" value="Enviar"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

	</main>

<?php get_footer(); ?>
