<?php /* Template Name: Post Page Template */ get_header(); ?>

	<main class="l-main-content dd-mtop" role="main">

        <div class="l-container__wrapper">
            <section class="l-container__with-sidebar">
                <div class="l-grid__blog l-grid__blog--single">
                   <h1 class="dd-title">Aquisição de duas usinas hidrelétricas da Gerdau</h1>

                   <div class="dd-breadcrumb">
                       <ul>
                           <li>
                               <a href="#">Home</a>
                           </li>
                            <li>
                                <span> > </span>
                            </li>
                           <li>
                               <a href="#">Blog</a>
                           </li>
                           <li>
                                <span> > </span>
                            </li>
                            <li>
                                <a href="#">Noticias</a>
                            </li>
                            <li>
                                <span> > </span>
                            </li>
                            <li>
                                <a href="#">Teste 123...</a>
                            </li>
                       </ul>
                   </div>

                   <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/blog.png'; ?>" alt=""/>

                    <div class="dd-info">
                        <div class="dd-content">
                            <!-- Date -->
                            <div class="dd-date">
                                <span class="dd-label">Publicado em:</span>
                                <p class="dd-text">12/11/2018</p>
                            </div>

                            <!-- Categorias -->
                            <div class="dd-categories">
                                <span class="dd-label">Categorias:</span>

                                <div class="dd-list">
                                    <span>Mercado</span>
                                    <span>, Artigos</span>
                                </div>
                            </div>
                        </div>

                        <div class="dd-share">
                            <span class="dd-label">Compartilhar:</span>

                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="dd-icon icon-share-facebook"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="dd-icon icon-share-twitter"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="dd-icon icon-share-email"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="dd-post-text">
                        <p>A Kinross Gold Corporation anunciou que a subsidiária Kinross Brasil Mineração 
                            concordou em adquirir duas usinas hidrelétricas da Gerdau S.A., no Brasil, pelo valor de 
                            US$ 257 milhões. Com a aquisição, a empresa espera assegurar o suprimento de energia, em 
                            longo prazo, para a mina de Paracatu, resultando em menores custos de produção ao longo 
                            da vida útil da mina.</p>
                        <p>&nbsp;</p>
                        <p>As duas usinas adquiridas da Gerdau são Barra dos Coqueiros e Caçu, localizadas no rio Claro, 
                            nas proximidades do estado de Goiás, a 660 km a oeste de Paracatu. Não serão necessários 
                            investimentos adicionais, uma vez que já existe a infraestrutura de transmissão e utilização da 
                            energia. As duas hidrelétricas estão em operação desde 2010 e têm capacidade para gerar, no total, 
                            155MW, sedo 90 MW da BCQ e 65MW de Caçu. As concessões de ambas usinas valem até 2037, quando 
                            deverá se encerrar a vida útil da mina.</p>
                            <p>&nbsp;</p>
                        <p>Para custear a aquisição, a Kinross fará um empréstimo de US$ 200 milhões, valor que considera 
                            compatível com a sua atual liquidez, que totalizou aproximadamente US$ 2,6 bilhões no final de 2007.</p>
                            <p>&nbsp;</p>
                        <p>
                            <strong>Resultados da Kinross</strong>
                        </p>
                        <p>A Kinross registrou uma redução em sua produção de ouro equivalente no quarto trimestre de 2017, 
                            com um total de 652.710 onças, contra 746.291 onças no mesmo período de 2016. A receita também 
                            caiu, passando de US$ 902,8 milhões para US$ 810,3 milhões. Mesmo assim, a empresa obteve um lucro 
                            de US$ 217,6 milhões, em comparação com uma perda de US$ 116,5 milhões em igual período do ano anterior.</p>
                            <p>&nbsp;</p>
                        <p>Para 2018, a empresa projeta uma produção de 2,5 milhões de onças de ouro equivalente, a um custo de 
                            produção de US$ 730 por onça e custo total de US$ 975/onça. O capex previsto para o ano é de US$ 1,075 
                            bilhão, incluindo US$ 355 milhões para sustaining das operações e US$ 680 milhões em projetos de desenvolvimento.</p>
                            <p>&nbsp;</p>
                        <p>Fonte: Brasil Mineral</p>
                    </div>

                </div>

                <!-- #aside -->
                <?php get_template_part('/includes/partials/asides/blog'); ?>
            </section>
        </div>
	</main>

<?php get_footer(); ?>
