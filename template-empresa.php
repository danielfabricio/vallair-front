<?php /* Template Name: Empresa Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">

        <!-- #section - Top Banner -->
        <?php get_template_part('/includes/partials/sections/top-video-banner'); ?>


        <!-- #section - Container -->
        <section class="l-grid__about">
            <div class="l-container__wrapper">
                <div class="l-grid__about__content">
                    <p>A Vallair é uma empresa brasileira fundada em 1987 e pioneira na fabricação 
                        de bombas pneumáticas e peristálticas no Brasil. Em sua trajetória, 
                        agregou a linha de produto analítica.</p>
                    <p>&nbsp;</p>
                    <p>A equipe de engenharia de aplicação e técnicos da Vallair comercializam 
                        estes equipamentos industriais com atendimento diferenciado em todas as 
                        fases do processo de aquisição do produto: visitas técnicas, projeto de 
                        conjuntos, dimensionamento e especificação, instalação e treinamento de manutenção.</p>
                    <p>&nbsp;</p>
                    <p>A Vallair possui estoque de peças de reposição estratégico para atender às 
                        demandas de consertos prontamente. O pós-vendas é uma das prioridades da 
                        equipe, que disponibiliza rapidamente vistas explodidas das bombas, manuais e 
                        informações sobre instalação e operação. A oficina de manutenção da Vallair entrega as 
                        bombas consertadas, pintadas e testadas em estado de novas, inclusive de marcas de 
                        bombas similares.</p>
                    <p>&nbsp;</p>
                    <p>A empresa é certificada ISO 9001:2008 e mantém busca de melhoria contínua de seus 
                        procedimentos, Kaizen, para atender o mercado industrial de maneira eficiente. A 
                        experiência com deslocamento e controle de fluidos desenvolvida ao longo de mais de 30 
                        anos faz com que a Vallair seja uma empresa altamente capacitada e que entende a 
                        necessidade, agilidade e versatilidade que o mercado brasileiro exige de um fornecedor 
                        para fazer frente aos desafios de eficiência e resultados que ele precisa atingir.</p>
                </div>
            </div>
        </section>


        <!-- #section - Certificates -->
        <?php get_template_part('/includes/partials/sections/certificates'); ?>


        <!-- #section - Partners -->
        <?php get_template_part('/includes/partials/sections/partners'); ?>

	</main>

<?php get_footer(); ?>
