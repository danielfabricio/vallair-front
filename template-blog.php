<?php /* Template Name: Blog Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">
		<!-- #section - Top Banner -->
        <?php get_template_part('/includes/partials/sections/top-banner'); ?>

        <div class="l-container__wrapper">
            <section class="l-container__with-sidebar">
                <div class="l-grid__blog">

                    <div class="dd-content-box-list" data-page="2" data-type="blog-list">
                        <div class="dd-content-list">
                            <?php for($i = 0; $i < 2; $i++):
                                get_template_part('/includes/partials/cards/blog-list');
                            endfor; ?>
                        </div>

                        <!-- #section - Load More -->
                        <?php get_template_part('/includes/partials/extras/loadmore'); ?>

                    </div>
                </div>

                <!-- #aside -->
                <?php get_template_part('/includes/partials/asides/blog'); ?>
            </section>
        </div>
	</main>

<?php get_footer(); ?>
