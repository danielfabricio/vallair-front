<?php /* Template Name:  Peças Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">

        <!-- #section - Top Banner -->
        <?php get_template_part('/includes/partials/sections/top-banner'); ?>

        <!-- #section - List -->
        <?php for($j = 0; $j < 2; $j++): ?>
            <section class="l-grid__produtos">
                <div class="l-container__wrapper">
                    <div class="l-container__full-row">
                        <h2 class="l-grid__produtos__title">PEÇAS LOREM IPSUM</h2>

                        <div class="dd-content-box-list" data-page="2" data-type="produto">
                            <div class="l-grid__produtos__list dd-content-list">
                                <?php for($i = 0; $i < 4; $i++):
                                    get_template_part('/includes/partials/cards/produto');
                                endfor; ?>
                            </div>

                            <?php get_template_part('/includes/partials/extras/custom-nav'); ?>

                            <?php get_template_part('/includes/partials/extras/loadmore'); ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endfor; ?>


        <!-- #section - Banner extra -->
        <?php get_template_part('/includes/partials/sections/banner-extra'); ?>


         <!-- #section - List -->
         <?php for($j = 0; $j < 2; $j++): ?>
            <section class="l-grid__produtos">
                <div class="l-container__wrapper">
                    <div class="l-container__full-row">
                        <h2 class="l-grid__produtos__title">PEÇAS LOREM IPSUM</h2>

                        <div class="dd-content-box-list" data-page="2" data-type="produto">
                            <div class="l-grid__produtos__list dd-content-list">
                                <?php for($i = 0; $i < 4; $i++):
                                    get_template_part('/includes/partials/cards/produto');
                                endfor; ?>
                            </div>

                            <?php get_template_part('/includes/partials/extras/custom-nav'); ?>

                            <?php get_template_part('/includes/partials/extras/loadmore'); ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endfor; ?>
	</main>

<?php get_footer(); ?>
