<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <!-- <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed"> -->

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

		<!-- header -->
		<header class="l-header">
			<div class="l-header__row l-header__row--top">
				<div class="l-container__wrapper">
					<div class="dd-col dd-col--left">
						<a href="<?php echo site_url(); ?>">
							<img class="dd-logo" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/logo.png'; ?>" alt="Vallair">
						</a>

						<button id="openMenuMobile" class="dd-menu-btn">
							<span></span>
							<span></span>
							<span></span>
						</button>

						<div class="dd-languages">
							<a href="#">
								<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/brasil.svg'; ?>" alt="Brasil">
							</a>

							<a href="#">
								<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/espanhol.svg'; ?>" alt="Espanhol">
							</a>
						</div>
					</div>

					<div class="dd-col dd-col--right">
						<a href="" class="m-button m-button--custom">
							<i class="dd-icon icon-phone"></i>

							<div class="dd-text">
								<span class="dd-label">fale conosco</span>
								<span class="dd-phone">(11) 2696-3411</span>
							</div>
						</a>
					</div>
				</div>

				<div class="l-header__menu-overlay"></div>
				<div id="menuMobile" class="l-header__menu-mobile">
					<button class="dd-btn-close">
						Fechar <i class="dd-icon icon-close"></i>
					</button>

					<div class="dd-header">
						<div class="dd-label">
							Escolha um idioma:
						</div>

						<div class="dd-languages">
							<a href="#">
								<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/brasil.svg'; ?>" alt="Brasil">
							</a>

							<a href="#">
								<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/espanhol.svg'; ?>" alt="Espanhol">
							</a>
						</div>
					</div>

					<nav>
						<ul>
							<li>
								<a class="menu-item-has-children" href="#">Produtos e Peças</a>

								<ul class="sub-menu">
									<li>
										<a href="#">Produtos Vallair</a>
									</li>
									<li>
										<a href="#">Pecas de Reposicao</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Serviços</a>
							</li>

							<li>
								<a href="#">Blog</a>
							</li>

							<li>
								<a href="#">Empresa</a>
							</li>

							<li>
								<a href="#">Contatos</a>
							</li>
						</ul>
					</nav>
					
					<div class="dd-footer">
						<a href="" class="m-button m-button--custom">
							<i class="dd-icon icon-phone"></i>

							<div class="dd-text">
								<span class="dd-label">fale conosco</span>
								<span class="dd-phone">(11) 2696-3411</span>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="l-header__row l-header__row--bottom">
				<div class="l-container__wrapper">
					<nav class="l-header__navigation">
						<ul>
							<li>
								<a class="menu-item-has-children" href="#">Produtos e Peças</a>

								<ul class="sub-menu">
									<li>
										<a href="#">
											<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/intro-thumb.png'; ?>" />

											<div class="dd-content">
												<h2 class="dd-title">Peças de reposição</h2>
												<div class="dd-description">
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus maximus 
													tellus vitae mollis sodales. Morbi posuere nisl vitae odio sagittis ultricies.
												</div>
											</div>
										</a>
									</li>

									<li>
										<a href="#">
											<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/intro-thumb.png'; ?>" />

											<div class="dd-content">
												<h2 class="dd-title">Peças de reposição</h2>
												<div class="dd-description">
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus maximus 
													tellus vitae mollis sodales. Morbi posuere nisl vitae odio sagittis ultricies.
												</div>
											</div>
										</a>
									</li>
								</ul>
							</li>

							<li>
								<a href="#">Serviços</a>
							</li>

							<li>
								<a href="#">Blog</a>
							</li>

							<li>
								<a href="#">Empresa</a>
							</li>

							<li>
								<a href="#">Contatos</a>
							</li>
						</ul>
					</nav>

					<button class="l-header__search-btn">
						<span>buscar</span>
						<i class="dd-icon icon-search"></i>
					</button>
				</div>
			</div>

			<div class="l-header__row l-header__row--search">
				<div class="l-container__wrapper">
					<form action="<?php echo site_url(); ?>">
						<input class="dd-input" type="text" placeholder="O QUE VOCÊ PROCURA?" name="s"/>

						<input type="submit" class="m-button m-button--simple" value="Buscar"/>
					</form>
				</div>
			</div>
		</header>
		<!-- /header -->
