<?php /* Template Name: Peca Interna Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">

        <!-- #section - Top Banner -->
        <?php get_template_part('/includes/partials/sections/top-banner'); ?>


        <!-- #section - Container -->
        <section class="l-grid__single">
            <div class="l-container__wrapper">
                <div class="l-grid__single__content">
                    <div class="dd-col dd-col--thumb">
                        <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() .'/assets/images/montagem/intro-thumb.png'; ?>" alt=""/>
                    </div>

                    <div class="dd-col dd-col--content">
                        <h2 class="dd-title">Lorem ipsum dollor</h2>

                        <div class="dd-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu libero elit. 
                                Morbi in eros leo. Aliquam risus nisi, maximus quis lobortis a, dignissim eu augue. 
                                Etiam aliquet, neque ornare cursus porttitor, metus magna venenatis ex, non malesuada 
                                risus nisl vel tortor. Suspendisse potenti. Cras id pharetra ex, sit amet accumsan 
                                ipsum. Donec ut arcu rutrum lorem molestie semper non.</p>
                            <p>&nbsp;</p>
                            <p>Pellentesque pulvinar convallis cursus. Sed ac lacus vehicula, dapibus erat non, eleifend 
                                lorem. Integer aliquet nisl eu dapibus ornare. Proin lorem odio, maximus eu ex at, porta 
                                sollicitudin felis. Phasellus tincidunt nibh dolor, eget elementum sem accumsan sed. Aliquam 
                                venenatis blandit diam sed convallis. Fusce in volutpat lectus.</p>
                        </div>

                        <a href="#" class="m-button m-button--default">
                            FAçA UM ORÇAMENTO
                        </a>
                    </div>
                </div>

                <div class="l-grid__single__advantage">
                    <div class="dd-col dd-col--content">
                        <h2 class="dd-title">Vantagens</h2>

                        <div class="dd-text">
                            <p>Funcionam a seco.</p>
                            <p>São auto-escorvantes.</p>
                            <p>Possuem peças intercambiáveis entre si.</p>
                            <p>Não necessitam de lubrificação.</p>
                            <p>Funcionam sem interrupção, mesmo com baixa pressão de alimentação de ar.</p>
                            <p>Possuem motor pneumático de aço inoxidável resistente à corrosão.</p>
                            <p>Possuem sistema pneumático anti-congelamento.</p>
                            <p>Não utilizam selos mecânicos.</p>
                            <p>São à prova de vazamento.</p>
                            <p>São portáteis e fáceis de instalar, ideais para uso em vários locais.</p>
                        </div>
                    </div>

                    <div class="dd-col dd-col--thumb">
                        <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() .'/assets/images/montagem/intro-thumb.png'; ?>" alt=""/>
                    </div>
                </div>
            </div>
        </section>


        <!-- #section - Banner extra -->
        <?php get_template_part('/includes/partials/sections/banner-extra'); ?>


        <!-- #section - Produtos featured list -->
        <?php get_template_part('/includes/partials/sections/featured-produtos'); ?>
	</main>

<?php get_footer(); ?>
