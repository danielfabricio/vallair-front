<?php get_header(); ?>

	<main class="l-main-content" role="main">
        <section class="l-container l-container--blog">
            <?php $currentCategoria = get_the_category(); ?>

            <h1 class="l-grid__title">Blog</h1>

            <?php $categorias = get_categories(); ?>
            <?php if($categorias): ?>
                <div class="l-container--blog__categorias">
                    <div class="l-grid l-grid--wrapper">
                        <ul class="dd-list">
                            <?php foreach($categorias as $categoria): ?>
                                <li class="dd-categoria">
                                    <a href="<?php echo get_category_link($categoria); ?>" 
                                        title="<?php echo $categoria->name; ?>" 
                                        class="<?php if ($categoria->cat_ID == $currentCategoria[0]->cat_ID) echo 'is-current'; ?>">
                                            <?php echo $categoria->name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>

            <?php $postList = getPagedPostsList(get_option('posts_per_page'), 1, $currentCategoria[0]->cat_ID); ?>
            <div class="l-container--blog__content m-ajax-list__container">
                <div class="l-grid l-grid--wrapper">
                    <div class="l-container--blog__list m-ajax-list" data-category="<?php echo $currentCategoria[0]->cat_ID; ?>">
                        <?php while($postList->have_posts()): $postList->the_post();
                            get_template_part('includes/partials/blog-list');
                        endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
	</main>

<?php get_footer(); ?>
