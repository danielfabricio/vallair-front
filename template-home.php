<?php /* Template Name: Home Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">
		<!-- #section - Banner -->
		<?php get_template_part('/includes/partials/sections/banner'); ?>
		

		<!-- #section - Intro -->
		<?php get_template_part('/includes/partials/sections/intro'); ?>


		<!-- #section - Service -->
		<?php get_template_part('/includes/partials/sections/services'); ?>


		<!-- #section - About -->
		<?php get_template_part('/includes/partials/sections/about'); ?>


		<!-- #section - Blog -->
		<?php get_template_part('/includes/partials/sections/blog'); ?>
	</main>

<?php get_footer(); ?>
