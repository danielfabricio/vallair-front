<?php

function custom_loadmore_ajax_handler() {
    $type = isset($_POST['type']) ? $_POST['type'] : '';

    if ($type != ''):
        for ($i = 0; $i < 4; $i++):
            get_template_part('/includes/partials/cards/' . $type);
        endfor;
    endif;

	die; // here we exit the script and even no wp_reset_query() required!
}
 
add_action('wp_ajax_loadmore', 'custom_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'custom_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

?>