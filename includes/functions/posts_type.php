<?php

// Create Posts Type
function custom_posts_type() {
    // Banners
    register_post_type('banners', // Register Custom Post Type
    array(
        'labels' => array(
            'name' => __('Banners'), // Rename these to suit
            'singular_name' => __('Banner'),
            'add_new' => __('Adicionar Nova'),
            'add_new_item' => __('Adicionar Nova Banner'),
            'edit' => __('Editar'),
            'edit_item' => __('Editar Banner'),
            'new_item' => __('Nova Banner'),
            'view' => __('Visualizar'),
            'view_item' => __('Visualizar Banner'),
            'search_items' => __('Pesquisar Banners'),
            'not_found' => __('Nenhum banner encontrado'),
            'not_found_in_trash' => __('Nenhum banner encontrado')
        ),
        'menu_icon' => 'dashicons-format-gallery',
        'public' => true,
        'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'supports' => array(
            'title',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
    ));
}

add_action('init', 'custom_posts_type'); // Add our HTML5 Blank Custom Post Type

?>