<?php


// Load custom assets
function custom_assets() {
    wp_enqueue_style( 'vendor_style', get_stylesheet_directory_uri() . '/assets/css/vendor.css' );
    wp_enqueue_style( 'app_style', get_stylesheet_directory_uri() . '/assets/css/app.css' );

    
    wp_enqueue_script( 'vendor_script', get_template_directory_uri() . '/assets/js/vendor.js', array(), '1.0.0', true );
    wp_enqueue_script( 'app_script', get_template_directory_uri() . '/assets/js/app.js', array(), '1.0.0', true );

    wp_localize_script( 'app_script', 'custom_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
	) );
 
}

add_action( 'wp_enqueue_scripts', 'custom_assets' );

?>