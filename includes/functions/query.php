<?php

/**
 * Custom Queries
 */

// Retorna Banners
function getBanners($qtd = -1) {
    $args = array(
        'post_type' => 'banners',
        'post_status' => 'publish',
        'posts_per_page' => $qtd
    );

    $posts = new WP_Query($args);

    wp_reset_postdata();

    return $posts;
}

?>