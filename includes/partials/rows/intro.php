<div class="l-container__intro__row">
    <div class="l-container__intro__col l-container__intro__col--text">
        <h2 class="dd-title">Peças de reposição</h2>
        <span class="dd-subtitle">para bombas pneumaticas e peristálticas</span>

        <div class="dd-description l-container__custom-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis semper. 
                Phasellus ut consequat nibh. Cras accumsan consectetur diam, at vulputate felis pretium vel.</p>
        </div>

        <a href="#" class="m-button m-button--default">
            Faça um Orçamento
        </a>
    </div>

    <div class="l-container__intro__col l-container__intro__col--thumb">
        <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/intro-thumb.png'; ?>" alt="">
    </div>
</div>