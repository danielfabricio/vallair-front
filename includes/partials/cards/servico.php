<article class="m-card m-card--service">
    <div class="dd-col dd-col--content">
        <h2 class="dd-title">Assistência técnica</h2>

        <div class="dd-description">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis semper. 
                Phasellus ut consequat nibh. Cras accumsan consectetur diam, at vulputate felis pretium 
                vel. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis 
                semper. Phasellus ut consequat nibh.</p>
            <p>&nbsp;</p>
            <p>Cras accumsan consectetur diam, at vulputate felis pretium vel.Lorem ipsum dolor sit 
                amet, consectetur adipiscing elit. Etiam commodo facilisis semper. Phasellus ut consequat 
                nibh. Cras accumsan consectetur diam, at vulputate felis pretium vel.</p>
        </div>

        <a href="#" class="m-button m-button--default">
            FAçA UM ORÇAMENTO
        </a>
    </div>

    <div class="dd-col dd-col--thumb">
        <img class="dd-img" src="<?php echo get_template_directory_uri() .'/assets/images/montagem/intro-thumb.png'; ?>" alt=""/>
    </div>
</article>