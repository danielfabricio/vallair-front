<div class="m-card m-card--blog-list">
    <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/blog.png'; ?>" alt=""/>

    <h2 class="dd-title">Aquisição de duas usinas hidrelétricas da Gerdau</h2>

    <div class="dd-description">
        A Kinross Gold Corporation anunciou que a subsidiária Kinross Brasil Mineração concordou em 
        adquirir duas usinas hidrelétricas da Gerdau S.A., no Brasil, pelo valor de US$ 257 milhões. 
        Com a aquisição, a empresa espera assegurar o suprimento de energia, em longo prazo, para a 
        mina de Paracatu, resultando em menores custos de produção ao longo da vida útil da 
        mina... <a href="#" class="dd-link">Continue lendo</a>
    </div>

    <div class="dd-footer">
        <div class="dd-content">
            <!-- Date -->
            <div class="dd-date">
                <span class="dd-label">Publicado em:</span>
                <p class="dd-text">12/11/2018</p>
            </div>

            <!-- Categorias -->
            <div class="dd-categories">
                <span class="dd-label">Categorias:</span>

                <div class="dd-list">
                    <span>Mercado</span>
                    <span>, Artigos</span>
                </div>
            </div>
        </div>

        <div class="dd-share">
            <span class="dd-label">Compartilhar:</span>

            <ul>
                <li>
                    <a href="#">
                        <i class="dd-icon icon-share-facebook"></i>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="dd-icon icon-share-twitter"></i>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="dd-icon icon-share-email"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>