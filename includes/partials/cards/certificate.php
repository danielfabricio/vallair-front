<div class="m-card m-card--certificates">
    <div class="dd-thumb">
        <img class="dd-img" src="<?php echo get_template_directory_uri() .'/assets/images/montagem/certificado.png'; ?>" alt=""/>
    </div>

    <div class="dd-description">
        A Vallair do Brasil é certificada <strong>ISO 9001:2008</strong> e busca constantemente inovação 
        e melhor eficiência para seu Sistema de Gestão de Qualidade através do PDCA 
        (Plan-Do-Check-Action) e da filosofia Kaizen (melhoria contínua).
    </div>
</div>