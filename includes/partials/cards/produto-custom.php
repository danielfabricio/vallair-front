<article class="m-card m-card--produto-custom">
    <div class="dd-content">
        <div class="dd-thumb">
            <img src="<?php echo get_template_directory_uri() . '/assets/images/montagem/intro-thumb.png'; ?>" alt=""/>
        </div>
        
        <h2 class="dd-title">BPV</h2>
        <div class="dd-description">
            <p>
                <strong>Vazão:</strong> até 52.680 l/h
            </p>
            <p>
                <strong>Pressão:</strong> até 15,7 bar
            </p>
            <p>
                <strong>Temperatura:</strong> 100°c
            </p>
            <p>
                <strong>Viscosidade:</strong> 25.000 cPs
            </p>
            <p>
                <strong>Sólidos:</strong> 9,4 mm
            </p>
        </div>

        <a href="#" class="m-button m-button--custom-primary">
            <span>Baixar PDF</span>
            <i class="dd-icon icon-download"></i>
        </a>
    </div>
</article>