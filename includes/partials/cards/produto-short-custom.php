<article class="m-card m-card--produto-short-custom">
    <div class="dd-content">
        <div class="dd-thumb">
            <img src="<?php echo get_template_directory_uri() . '/assets/images/montagem/intro-thumb.png'; ?>" alt=""/>
        </div>
        
        <h2 class="dd-title">Automotivo</h2>
        <div class="dd-description">
            transferência de óleos lubrificantes, combustíveis, fluidos...
        </div>

        <button class="m-button m-button--short">+ detalhes</button>
    </div>
</article>