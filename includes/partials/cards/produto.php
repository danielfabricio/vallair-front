<article class="m-card m-card--produto">
    <a href="#" class="dd-link">
        <div class="dd-thumb">
            <img src="<?php echo get_template_directory_uri() . '/assets/images/montagem/intro-thumb.png'; ?>" alt=""/>
        </div>
        
        <h2 class="dd-title">Lorem ipsum dollor</h2>
        <div class="dd-description">
            Lorem ipsum dolor sit amet, con sectetur adipiscing elit.
        </div>

        <span class="m-button m-button--primary">Ver Produtos</span>
    </a>
</article>