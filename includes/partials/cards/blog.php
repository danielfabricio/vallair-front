<article class="m-card m-card--blog">
    <a class="dd-link" href="#">
        <div class="dd-thumb">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/blog-thumb.png'; ?>" alt="">
        </div>

        <div class="dd-content">
            <h2 class="dd-title">Lorem ipsum dollor melt</h2>

            <div class="dd-description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis.
            </div>

            <span class="dd-date">22 de agosto de 2018</span>
        </div>
    </a>
</article>