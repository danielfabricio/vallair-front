<section class="l-container__partners">
    <div class="l-container__wrapper">
        <h2 class="l-container__partners__title">Parceiros</h2>
    </div>

    <div id="partnersCarousel" class="l-container__partners__carousel">
        <?php for ($i = 0; $i < 10; $i++): ?>
            <div class="dd-item">
                <img class="dd-img" src="<?php echo get_template_directory_uri() .'/assets/images/montagem/parceiro.png'; ?>" alt="" />
            </div>
        <?php endfor; ?>
    </div>
</section>