<section class="l-container__banners">
    <div id="bannerHome" class="m-banner m-banner--home">
        <?php for ($i = 0; $i < 3; $i++):
            get_template_part('/includes/partials/cards/banner');
        endfor; ?>
    </div>
</section>