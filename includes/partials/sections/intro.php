<section class="l-container__intro">
    <div class="l-container__wrapper">
        <div class="l-container__intro__list">
            <?php for($i = 0; $i < 2; $i++):
                get_template_part('/includes/partials/rows/intro');
            endfor; ?>
        </div>
    </div>
</section>