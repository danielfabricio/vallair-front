<section class="l-grid__top-banner" style="background-image: url('<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/top-banner.jpg'; ?>');">
    <div class="dd-overlay"></div>

    <div class="dd-content">
        <div class="l-container__wrapper">
            <h1 class="dd-title">Produtos Vallair</h1>
            
            <div class="dd-breadcrumb">
                <ul>
                    <li class="dd-item">
                        <a class="dd-link dd-link--home" href="<?php echo site_url(); ?>">Home</a>
                    </li>
                    <li class="dd-item">
                        <span class="dd-link">></span>
                    </li>
                    <li class="dd-item">
                        <a class="dd-link" href="#">Produtos</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>