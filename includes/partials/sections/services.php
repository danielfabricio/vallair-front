<section class="l-container__services">
    <div class="l-container__wrapper">
        <div class="l-container__services__content">
            <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-servicos.png'; ?>" alt="">

            <div class="dd-content">
                <div class="dd-info">
                    <span class="dd-obs">Lorem Ipsum</span>
                    <h2 class="dd-title">SERVIÇOS</h2>

                    <div class="dd-description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis semper. Phasellus ut consequat nibh.
                    </div>
                </div>

                <a href="#" class="m-button m-button--assertive">
                    CONTRATE UM SERVIÇO
                </a>
            </div>
        </div>
    </div>
</section>