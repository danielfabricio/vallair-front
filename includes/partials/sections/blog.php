<section class="l-container__blog">
    <div class="l-container__wrapper">
        <div class="l-container__blog__header">
            <h2 class="dd-title">Últimas do blog</h2>

            <a href="#" class="dd-link">
                <span>Ver todas</span>
                <i class="dd-icon icon-plus"></i>
            </a>
        </div>

        <div class="l-container__blog__content">
            <div class="l-container__full-row">
                <div id="blogPosts">
                    <?php for($i = 0; $i < 4; $i++):
                        get_template_part('/includes/partials/cards/blog');
                    endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>