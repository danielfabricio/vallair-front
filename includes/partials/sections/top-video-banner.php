<section class="l-grid__top-video-banner">
    
    <div id="player">
        <iframe src="https://www.youtube.com/embed/C0DPdy98e4c?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" allowfullscreen allowtransparency allow="autoplay"></iframe>
    </div>

    <div class="dd-overlay"></div>

    <div class="dd-content">
        <div class="l-container__wrapper">
            <h1 class="dd-title">Produtos Vallair</h1>
            
            <div class="dd-breadcrumb">
                <ul>
                    <li class="dd-item">
                        <a class="dd-link dd-link--home" href="<?php echo site_url(); ?>">Home</a>
                    </li>
                    <li class="dd-item">
                        <span class="dd-link">></span>
                    </li>
                    <li class="dd-item">
                        <a class="dd-link" href="#">Produtos</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>