<section class="l-container__featured-list">
    <div class="l-container__wrapper">
        <h2 class="dd-content-title">Conheça nossas peças de reposição</h2>

        <div class="l-container__full-row">

            <div id="productsCarousel" class="l-grid__featured-list">
                <?php for ($i = 0; $i < 3; $i++):
                    get_template_part('/includes/partials/cards/produto-short');
                endfor; ?>

                <div class="m-card m-card--link-page">
                    <a href="#" class="dd-link">
                        <h2 class="dd-title">Ver todas nossas peças de reposição</h2>

                        <span class="m-button m-button--card">
                            VER PEÇAS <i class="dd-icon icon-arrow-right"></i>
                        </span>
                    </a>
                </div>
            </div>

            <?php get_template_part('/includes/partials/extras/custom-nav'); ?>

        </div>
    </div>
</section>