<section class="l-container__about">
    <div class="l-container__wrapper">
        <div class="l-container__about__content">
            <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/img-sobre.png'; ?>" alt="">

            <div class="dd-info">
                <span class="dd-obs">Lorem Ipsum</span>
                <h2 class="dd-title">Nossa Empresa</h2>

                <div class="dd-description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam commodo facilisis semper. Phasellus ut consequat nibh. 
                </div>

                <a href="" class="m-button m-button--default dd-tiny">Saiba Mais</a>
            </div>
        </div>
    </div>
</section>