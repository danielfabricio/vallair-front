<section class="l-container__certificates">
    <div class="l-container__wrapper">
        <h2 class="dd-container-title">Certificados</h2>

        <div class="l-container__certificates__list">
            <div class="dd-list">
                <?php for($i = 0; $i < 2; $i++):
                    get_template_part('/includes/partials/cards/certificate');
                endfor; ?>
            </div>
        </div>
    </div>
</section>