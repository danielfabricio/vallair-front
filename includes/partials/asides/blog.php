<aside class="m-aside__blog">

    <!-- #widget - Categorias -->
    <div class="m-widget__categorias m-aside__blog__box">
        <h2 class="dd-container-title">Categorias</h2>

        <ul class="dd-list">
            <li>
                <a href="#">Todas as noticias</a>
            </li>

            <li>
                <a href="#">Artigos Tecnicos</a>
            </li>
            
            <li>
                <a href="#">Projetos Sociais</a>
            </li>
        </ul>
    </div>

    <!-- #widget - Redes Sociais -->
    <div class="m-widget__socials m-aside__blog__box">
        <h2 class="dd-container-title">Redes Sociais</h2>

        <nav>
            <ul class="dd-socials">
                <li>
                    <a href="#">
                        <i class="dd-icon icon-facebook"></i>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="dd-icon icon-twitter"></i>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="dd-icon icon-youtube"></i>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="dd-icon icon-facebook"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

    <!-- #widget - Side Banner -->
    <div class="m-widget__side-banner">
        <a href="#">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/montagem/side-banner.png'; ?>" alt=""/>
        </a>
    </div>
    
</aside>