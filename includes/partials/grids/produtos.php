<div class="l-grid__custom-products">
    <h2 class="dd-container-title">Linha de produtos disponível</h2>

    <div class="dd-list">
        <?php for($i = 0; $i < 6; $i++): 
            get_template_part('/includes/partials/cards/produto-custom');
        endfor; ?>
    </div>

    <?php get_template_part('/includes/partials/extras/custom-nav'); ?>
</div>