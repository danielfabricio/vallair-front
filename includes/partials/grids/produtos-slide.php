<div class="l-grid__produtos-slide">
    <h2 class="dd-container-title">Linha de produtos disponível</h2>

    <div class="dd-list">
        
        <div id="produtosSlide" class="dd-slide">
            <?php for($i = 0; $i < 10; $i++): 
                get_template_part('/includes/partials/cards/produto-short-custom');
            endfor; ?>
        </div>

        <?php get_template_part('/includes/partials/extras/custom-nav'); ?>
    </div>
</div>