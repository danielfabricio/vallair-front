<?php /* Template Name: Servicos Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">

        <!-- #section - Top Banner -->
        <?php get_template_part('/includes/partials/sections/top-banner'); ?>


        <!-- #section - List -->
        <section class="l-grid__services">
            <div class="l-container__wrapper">
                <div class="l-grid__services__list">
                    <?php for($i = 0; $i < 3; $i++):
                        get_template_part('/includes/partials/cards/servico');
                    endfor; ?>
                </div>
            </div>
        </section>
	</main>

<?php get_footer(); ?>
