<?php /* Template Name: Contatos Page Template */ get_header(); ?>

	<main class="l-main-content" role="main">

        <!-- #section - Top Banner -->
        <?php get_template_part('/includes/partials/sections/top-banner'); ?>


        <!-- #section - Container -->
        <section class="l-grid__contact">
            <div class="l-container__wrapper">
                <div class="l-grid__contact__content">
                    <div class="dd-col dd-col--content">
                        <h2 class="dd-title">Vallair do Brasil</h2>

                        <h3 class="dd-subtitle">Endereço</h3>
                        <div class="dd-text">
                            <p>Vallair do Brasil Indústria e Comércio Ltda.</p>
                            <p>Rua Catarina Braida, 396 - Moóca</p>
                            <p>CEP: 03169-030 – São Paulo / SP</p>
                        </div>

                        <h3 class="dd-subtitle">Emails</h3>
                        <div class="dd-text">
                            <p>Solicitações gerais: <a href="mailto:vallair@vallair.com.br">vallair@vallair.com.br</a></p>
                            <p>Comercial: <a href="mailto:vendas@vallair.com.br">vendas@vallair.com.br</a></p>
                        </div>

                        <a href="tel:+551126963411" class="dd-btn dd-btn--phone">
                            <i class="dd-icon icon-phone"></i>
                            <span>(11) 2696.3411</span>
                        </a>

                        <br/>

                        <a href="#" class="dd-btn dd-btn--whatsapp">
                            <i class="dd-icon icon-whatsapp"></i>
                            <span>(11) 99458.2177</span>
                        </a>
                    </div>

                    <div class="dd-col dd-col--form">

                        <?php if (!isset($_GET['msg']) || $_GET['msg'] == ''): ?>
                            <h2 class="dd-title">Fale Conosco</h2>

                            <div class="m-form">
                                <form>
                                    <div class="m-form__input">
                                        <label class="dd-label" for="">Seu nome</label>
                                        <input class="dd-input" type="text" name="nome"/>
                                    </div>

                                    <div class="m-form__input">
                                        <label class="dd-label" for="">Email</label>
                                        <input class="dd-input" type="text" name="email"/>
                                    </div>

                                    <div class="m-form__input">
                                        <label class="dd-label" for="">Telefone</label>
                                        <input class="dd-input" type="text" name="telefone"/>
                                    </div>

                                    <div class="m-form__input m-form__input--textarea">
                                        <textarea class="dd-textarea" type="text" name="mensagem" placeholder="Mensagem"></textarea>
                                    </div>

                                    <input class="dd-submit" type="submit" value="Enviar"/>
                                </form>
                            </div>
                        <?php else: ?>
                            <div class="dd-success-msg">
                                <h2 class="dd-title">Formulário enviado com sucesso</h2>

                                <div class="dd-text">
                                    Seu formulário foi enviado corretamente, em breve entraremos em contato. Obrigado.
                                </div>

                                <img class="dd-img" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/success.png'; ?>" alt=""/>

                                <a href="<?php echo site_url() .'/contato/'; ?>" class="m-button m-button--extra dd-big">
                                    Enviar nova Mensagem
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>


        <!-- #section - Location -->
        <?php get_template_part('/includes/partials/sections/location'); ?>

	</main>

<?php get_footer(); ?>
